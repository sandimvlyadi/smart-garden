<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\ParamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home/{device}/{type}', [HomeController::class, 'type'])->name('home.type');

Route::resource('devices', DeviceController::class);
Route::get('devices/{device}/data', [DeviceController::class, 'data'])->name('devices.data');
Route::get('devices/{device}/data-json', [DeviceController::class, 'dataJson'])->name('devices.data-json');

Route::resource('params', ParamController::class);
