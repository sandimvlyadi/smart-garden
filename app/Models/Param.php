<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    use HasFactory;

    protected $table = 'params';

    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return 'Active';
        } elseif ($this->status == 2) {
            return 'Inactive';
        } else {
            return '';
        }
    }
}
