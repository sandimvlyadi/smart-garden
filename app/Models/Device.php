<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Device extends Model
{
    use HasFactory;

    protected $table = 'devices';

    public function getWebhookAttribute()
    {
        return url("/api/webhook/{$this->appid}/{$this->appsecret}");
    }

    public static function generateappid()
    {
        $result = '';
        $repeat = true;
        while ($repeat) {
            $result = strtoupper(Str::random(8));
            $record = self::where('appid', $result)->first();
            if (is_null($record)) {
                $repeat = false;
            }
        }

        return $result;
    }

    public static function generateappsecret()
    {
        $result = '';
        $repeat = true;
        while ($repeat) {
            $result = strtoupper(Str::random(16));
            $record = self::where('appsecret', $result)->first();
            if (is_null($record)) {
                $repeat = false;
            }
        }

        return $result;
    }

    public function data()
    {
        return $this->hasMany(DeviceData::class, 'device_id');
    }
}
