<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\DeviceData;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $devices = Device::where('created_by', $user->id)
                        ->orderBy('id', 'desc')
                        ->get();

        return view('home', [
            'devices' => $devices
        ]);
    }

    public function type(Device $device, $type = '')
    {
        if (!in_array($type, ['suhu', 'tanah', 'udara', 'cahaya'])) {
            return response()->json([
                'message' => 'Bad Request!'
            ], 400);
        }

        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return response()->json([
                'message' => 'Device is not found!'
            ], 404);
        }

        $data = DeviceData::where('device_id', $device->id)
                          ->where('body', 'like', "%\"{$type}\"%")
                          ->orderBy('id', 'desc')
                          ->take(15)
                          ->get()
                          ->reverse()
                          ->values();
        
        $labels = [];
        $values = [];
        foreach ($data as $d) {
            $body = (array)json_decode($d->body);
            $labels[] = date('H:i:s', strtotime($d->created_at));
            $values[] = $body[$type];
        }
        
        return response()->json([
            'labels' => $labels,
            'values' => $values
        ], 200);
    }
}
