<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\StoreParamRequest;
use App\Http\Requests\UpdateParamRequest;
use App\Models\Param;

class ParamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $records = Param::where('created_by', $user->id)
                        ->orderBy('id', 'desc')
                        ->get();

        return view('param.index', [
            'records' => $records
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('param.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreParamRequest $request)
    {
        $user = Auth::user();

        $record = new Param;
        $record->name = $request->name;
        $record->value = $request->value;
        $record->status = $request->status;
        $record->created_by = $user->id;
        $record->save();

        return redirect(route('params.index'))->with('status', 'New param has been added!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Param $param)
    {
        $user = Auth::user();
        if ($param->created_by != $user->id) {
            return abort(404);
        }

        return view('param.show', [
            'record' => $param
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Param $param)
    {
        $user = Auth::user();
        if ($param->created_by != $user->id) {
            return abort(404);
        }

        return view('param.edit', [
            'record' => $param
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateParamRequest $request, Param $param)
    {
        $param->name = $request->name;
        $param->value = $request->value;
        $param->status = $request->status;
        $param->save();

        return redirect(route('params.index'))->with('status', 'Param has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Param $param)
    {
        $user = Auth::user();
        if ($param->created_by != $user->id) {
            return response()->json([
                'message' => 'Param is not found!'
            ], 404);
        }

        $param->delete();
        return response()->json([
            'message' => 'Param has been deleted!'
        ], 200);
    }
}
