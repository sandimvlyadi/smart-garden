<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Http\Requests\StoreDeviceRequest;
use App\Http\Requests\UpdateDeviceRequest;
use App\Models\Device;

class DeviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $records = Device::where('created_by', $user->id)
                         ->orderBy('id', 'desc')
                         ->get();

        return view('device.index', [
            'records' => $records
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('device.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDeviceRequest $request)
    {
        $user = Auth::user();

        $record = new Device;
        $record->name = $request->name;
        $record->appid = Device::generateappid();
        $record->appsecret = Device::generateappsecret();
        $record->created_by = $user->id;
        $record->save();

        return redirect(route('devices.index'))->with('status', 'New device has been added!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Device $device)
    {
        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return abort(404);
        }

        return view('device.show', [
            'record' => $device
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Device $device)
    {
        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return abort(404);
        }

        return view('device.edit', [
            'record' => $device
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDeviceRequest $request, Device $device)
    {
        $device->name = $request->name;
        $device->save();

        return redirect(route('devices.index'))->with('status', 'Device has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Device $device)
    {
        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return response()->json([
                'message' => 'Device is not found!'
            ], 404);
        }

        $device->delete();
        return response()->json([
            'message' => 'Device has been deleted!'
        ], 200);
    }

    public function data(Device $device)
    {
        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return abort(404);
        }

        return view('device.data', [
            'device' => $device
        ]);
    }

    public function dataJson(Request $request, Device $device)
    {
        $limit = $request->limit ? (int)$request->limit : 10;
        $offset = $request->offset ? (int)$request->offset : 0;

        $user = Auth::user();
        if ($device->created_by != $user->id) {
            return response()->json([
                'message' => 'Device is not found!'
            ], 404);
        }

        return response()->json([
            'data' => $device->data()->orderBy('id', 'desc')->limit($limit)->offset($offset)->get()
        ], 200);
    }
}
