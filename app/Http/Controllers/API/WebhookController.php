<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\DeviceData;

class WebhookController extends Controller
{
    public function store(Request $request, $appid = '', $appsecret = '')
    {
        $device = Device::where('appid', $appid)
                        ->where('appsecret', $appsecret)
                        ->first();

        if ($device) {
            $data = new DeviceData;
            $data->device_id = $device->id;
            $data->body = json_encode($request->all());
            $data->save();

            return response()->json($data, 200);
        }
        
        return response()->json([
            'message' => 'Device is not found!'
        ], 404);
    }
}
