<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Param;

class ParamController extends Controller
{
    public function index(Request $request, $appid = '', $appsecret = '')
    {
        $device = Device::where('appid', $appid)
                        ->where('appsecret', $appsecret)
                        ->first();

        if ($device) {
            $data = Param::select(['name', 'value'])
                         ->where('created_by', $device->created_by)
                         ->where('status', 1)
                         ->orderBy('id', 'desc')
                         ->get();

            return response()->json($data, 200);
        }
        
        return response()->json([
            'message' => 'Device is not found!'
        ], 404);
    }
}
