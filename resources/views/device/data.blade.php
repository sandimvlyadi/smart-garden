@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{$device->name}}</div>
                <div class="card-body" style="max-height: 80vh; overflow: auto;">
                    <div class="row gap-2">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="5%">No.</th>
                                        <th>Body</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                    <tr id="trEmpty">
                                        <td colspan="99" class="text-center">No Data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 text-center">
                            <button id="btnLoad" type="button" class="btn btn-primary" onclick="loadmore()">Load More</button>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <a href="{{route('devices.index')}}" class="btn btn-secondary">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let limit = 10;
    let offset = 0;

    window.onload = function() {
        loadmore();
    }

    const toggleloading = (value) => {
        const button = document.getElementById('btnLoad');
        button.disabled = value;
        if (value) {
            button.innerText = 'Loading';
        } else {
            button.innerText = 'Load More';
        }
    }

    const removebuttonloading = () => {
        const button = document.getElementById('btnLoad');
        button.parentNode.remove();
    }

    const removetrempty = () => {
        const trEmpty = document.getElementById('trEmpty');
        if (trEmpty !== null) {
            trEmpty.remove();
        }
    }

    const loadmore = () => {
        toggleloading(true);
        axios.get("{{route('devices.data-json', $device->id)}}", {
            params: {
                limit: limit,
                offset: offset
            }
        })
        .then((res) => {
            const records = res.data.data;
            if (records.length) {
                removetrempty();
            }

            records.forEach(record => {
                const tbody = document.getElementById('tbody');
                const no = tbody.children.length+1;

                const tr = document.createElement('tr');
                const tdNo = document.createElement('td');
                tdNo.classList.add('text-center');
                tdNo.appendChild(document.createTextNode(no));
                const tdBody = document.createElement('td');
                tdBody.appendChild(document.createTextNode(record.body));
                const tdDate = document.createElement('td');
                const createdAt = new Date(record.created_at);
                tdDate.appendChild(document.createTextNode(moment(createdAt).format('D/MM/y hh:mm:ss')));
                tr.appendChild(tdNo);
                tr.appendChild(tdBody);
                tr.appendChild(tdDate);
                tbody.appendChild(tr);

                offset++;
                toggleloading(false);
            });

            if (records.length < limit) {
                removebuttonloading();
            }
        })
        .catch((err) => {
            Swal.fire({
                title: `${err.response.status} | ${err.response.statusText}`,
                text: err.message,
                icon: 'error',
                showConfirmButton: false,
                timer: 3000
            });
        });
    }
</script>
@endsection