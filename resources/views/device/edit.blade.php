@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form action="{{route('devices.update', $record->id)}}" method="post">
                @csrf
                @method('put')
                <input type="text" name="id" class="d-none" value="{{$record->id}}">
                <div class="card">
                    <div class="card-header">{{ __('Edit Device') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mb-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{$record->name}}" placeholder="Name" required>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="form-group">
                                    <label>APPID</label>
                                    <input type="text" name="appid" class="form-control" value="{{$record->appid}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="form-group">
                                    <label>APPSECRET</label>
                                    <input type="text" name="appsecret" class="form-control" value="{{$record->appsecret}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-12 mb-4">
                                <div class="form-group">
                                    <label>Webhook</label>
                                    <input type="text" name="webhook" class="form-control" value="{{$record->webhook}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <a href="{{route('devices.index')}}" class="btn btn-secondary">Back</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
