@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Devices') }}</div>
                <div class="card-body" style="max-height: 80vh; overflow: auto;">
                    <div class="row gap-2">
                        <div class="col-md-12">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <a href="{{route('devices.create')}}" class="btn btn-primary">Add Device</a>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="5%">No.</th>
                                        <th>Name</th>
                                        <th>Webhook</th>
                                        <th>Data</th>
                                        <th class="text-center" width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @forelse ($records as $record)    
                                        <tr>
                                            <td class="text-center">{{$i}}</td>
                                            <td>{{$record->name}}</td>
                                            <td>{{$record->webhook}}</td>
                                            <td><a href="{{route('devices.data', $record->id)}}">{{$record->data->count()}}</a> received</td>
                                            <td>
                                                <div class="d-flex justify-content-center gap-2">
                                                    <a href="{{route('devices.show', $record->id)}}" class="btn btn-sm btn-secondary">Show</a>
                                                    <a href="{{route('devices.edit', $record->id)}}" class="btn btn-sm btn-dark">Edit</a>
                                                    <a href="javascript:;" class="btn btn-sm btn-danger" onclick="handleDelete({{$record->id}})">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td colspan="99" class="text-center">No Data</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    const handleDelete = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            focusCancel: true,
            reverseButtons: true,
            showCancelButton: true,
            confirmButtonColor: '#dc3545',
            cancelButtonColor: '#6c757d',
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`/devices/${id}`)
                .then((res) => {
                    Swal.fire({
                        title: 'Success',
                        text: res.data.message,
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 3000
                    }).then(() => {
                        window.location.reload();
                    });
                })
                .catch((err) => {
                    Swal.fire({
                        title: `${err.response.status} | ${err.response.statusText}`,
                        text: err.message,
                        icon: 'error',
                        showConfirmButton: false,
                        timer: 3000
                    });
                });
            }
        });
    }
</script>
@endsection