@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body" style="max-height: 80vh; overflow: auto;">
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 mb-4">
                            <div class="form-group">
                                <label>Device</label>
                                <select id="device_id" name="device_id" class="form-control" placeholder="Select Device" onchange="handlechangedeviceid()">
                                    <option value="">Select Device</option>
                                    @foreach ($devices as $device)
                                        <option value="{{$device->id}}">{{$device->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <canvas id="suhu"></canvas>
                        </div>
                        <div class="col-md-6 mb-4">
                            <canvas id="tanah"></canvas>
                        </div>
                        <div class="col-md-6 mb-4">
                            <canvas id="udara"></canvas>
                        </div>
                        <div class="col-md-6 mb-4">
                            <canvas id="cahaya"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let deviceId = '';
    
    window.onload = function() {
        handlechangedeviceid();
    }

    const handlechangedeviceid = () => {
        deviceId = document.getElementById('device_id').value;
        
        if (deviceId.length > 0) {
            document.getElementById('suhu').style.display = 'block';
            document.getElementById('tanah').style.display = 'block';
            document.getElementById('udara').style.display = 'block';
            document.getElementById('cahaya').style.display = 'block';
        } else {
            document.getElementById('suhu').style.display = 'none';
            document.getElementById('tanah').style.display = 'none';
            document.getElementById('udara').style.display = 'none';
            document.getElementById('cahaya').style.display = 'none';
        }
    }
</script>

<script type="module">
    const idSuhu = document.getElementById('suhu');

    const labels = [];
    const data = [];
    const chartSuhu = new Chart(idSuhu, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Suhu',
                data: data,
                fill: false,
                borderColor: 'rgb(13, 110, 253)',
                tension: 0.5
            }]
        },
    });

    setInterval(() => {
        if (deviceId.length > 0) {
            axios.get(`/home/${deviceId}/suhu`)
            .then((res) => {
                const labels = res?.data?.labels;
                const values = res?.data?.values;

                chartSuhu.config.data.labels = labels;
                chartSuhu.data.datasets[0].data = values;
                chartSuhu.update();
            })
            .catch((err) => {
                console.log(err);
            });
    
        }
    }, 3000);
</script>

<script type="module">
    const idTanah = document.getElementById('tanah');

    const labels = [];
    const data = [];
    const chartTanah = new Chart(idTanah, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Tanah',
                data: data,
                fill: false,
                borderColor: 'rgb(25, 135, 84)',
                tension: 0.5
            }]
        },
    });

    setInterval(() => {
        if (deviceId.length > 0) {
            axios.get(`/home/${deviceId}/tanah`)
            .then((res) => {
                const labels = res?.data?.labels;
                const values = res?.data?.values;

                chartTanah.config.data.labels = labels;
                chartTanah.data.datasets[0].data = values;
                chartTanah.update();
            })
            .catch((err) => {
                console.log(err);
            });
    
        }
    }, 3000);
</script>

<script type="module">
    const idUdara = document.getElementById('udara');

    const labels = [];
    const data = [];
    const chartUdara = new Chart(idUdara, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Udara',
                data: data,
                fill: false,
                borderColor: 'rgb(255, 193, 7)',
                tension: 0.5
            }]
        },
    });

    setInterval(() => {
        if (deviceId.length > 0) {
            axios.get(`/home/${deviceId}/udara`)
            .then((res) => {
                const labels = res?.data?.labels;
                const values = res?.data?.values;

                chartUdara.config.data.labels = labels;
                chartUdara.data.datasets[0].data = values;
                chartUdara.update();
            })
            .catch((err) => {
                console.log(err);
            });
    
        }
    }, 3000);
</script>

<script type="module">
    const idCahaya = document.getElementById('cahaya');

    const labels = [];
    const data = [];
    const chartCahaya = new Chart(idCahaya, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Cahaya',
                data: data,
                fill: false,
                borderColor: 'rgb(13, 202, 240)',
                tension: 0.5
            }]
        },
    });

    setInterval(() => {
        if (deviceId.length > 0) {
            axios.get(`/home/${deviceId}/cahaya`)
            .then((res) => {
                const labels = res?.data?.labels;
                const values = res?.data?.values;

                chartCahaya.config.data.labels = labels;
                chartCahaya.data.datasets[0].data = values;
                chartCahaya.update();
            })
            .catch((err) => {
                console.log(err);
            });
    
        }
    }, 3000);
</script>
@endsection
