<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Device;
use App\Models\DeviceData;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $devices = Device::get();
        foreach ($devices as $device) {
            for ($i=0; $i < 10; $i++) { 
                $body = [
                    'tanah'     => mt_rand(0,1000) / 10,
                    'suhu'      => mt_rand(0,1000) / 10,
                    'udara'     => mt_rand(0,1000) / 10,
                    'cahaya'    => mt_rand(0,1000) / 10
                ];

                $data = new DeviceData;
                $data->device_id = $device->id;
                $data->body = json_encode($body);
                $data->save();
            }
        }
    }
}
