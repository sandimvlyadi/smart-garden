<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Device;
use App\Models\Param;

class ParamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $devices = Device::get();
        foreach ($devices as $device) {
            $data = new Param;
            $data->name = 'pagi_jam';
            $data->value = 8;
            $data->status = 1;
            $data->created_by = $device->created_by;
            $data->save();

            $data = new Param;
            $data->name = 'pagi_menit';
            $data->value = 30;
            $data->status = 1;
            $data->created_by = $device->created_by;
            $data->save();

            $data = new Param;
            $data->name = 'sore_jam';
            $data->value = 17;
            $data->status = 1;
            $data->created_by = $device->created_by;
            $data->save();

            $data = new Param;
            $data->name = 'sore_menit';
            $data->value = 30;
            $data->status = 1;
            $data->created_by = $device->created_by;
            $data->save();

            $data = new Param;
            $data->name = 'lampu';
            $data->value = 1;
            $data->status = 1;
            $data->created_by = $device->created_by;
            $data->save();
        }
    }
}
